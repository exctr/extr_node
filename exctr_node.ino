/*
 Copyright (C) 2011-14 Jari Suominen
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

/*
 Set the fuses of the ATMEGA328 like so:
 avrdude -P /dev/ttyACM0 -c avrispmkII -p m328p -U lfuse:w:0xff:m -U hfuse:w:0xdf:m -U efuse:w:0x07:m
*/

/*
                                      ,o
                                  }-=')-<
          .
         :                           ^^^
        (_)              ___         |||
         .  :          _(   ")       `+'
         . o          (  _)\ .{}{}}.  |
          .            )/ "( {}-_-{}  |
        O .          .-_) _) `,{=}._  |
        : o.        //   (.---{{{}}_)( \
          .   (.___/(_, .(   "-`}'-   T \
         O     "--._ |`- / \ _     _ /|  )
         .o         / .  | |`---^---'||-"
---.-""."-._____.-.'`-._.\^|\_  .:_/_|_.---"-""-.-."-.____.-.__
        .       :   ._  /\ ("-.___.-( |                   ,-.
         :     (      .'  \_)        \|7                 .' /
                :    (     \          |.                /  <
                 \    \    _`.      ) . "-._          .'/  |
                  `. )'`.-" /| /-._  )'    )""--.__.-','  /
                    `-._   < `(    ""--.__  )'        _  \
                        `.  |             """....---"" "._)  ojo
*/
 
#include "avr/pgmspace.h"

/* Here shall one enter the default address */
byte address = 0xFF;
boolean PARALLEL_MODE = false;
boolean broadcasting = false;

#define MAX_SERIAL_WAIT_MILLIS 100
#define RESET_DELAY 10 // The time to hold reseter pin low to reset the board connected to it.
#define BOOT_DELAY 1000  // The to be waited for the serial communication kick up after boot.
#define CONNECTION_LOST_TIMEOUT 600000 // After a while we will turn the speaker of if no data is received

#define EXCTR_LED 9
#define ARDUINO_LED 13
#define RESETER_PIN 12  // Can be changed to whatever

#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))

// table of 256 sine values / one sine period / stored in flash memory
PROGMEM const unsigned char sine256[]  = {
  127,130,133,136,139,143,146,149,152,155,158,161,164,167,170,173,176,
  178,181,184,187,190,192,195,198,200,203,205,208,210,212,215,217,219,
  221,223,225,227,229,231,233,234,236,238,239,240,
  242,243,244,245,247,248,249,249,250,251,252,252,253,253,253,254,254,
  254,254,254,254,254,253,253,253,252,252,251,250,249,249,248,247,245,
  244,243,242,240,239,238,236,234,233,231,229,227,225,223,
  221,219,217,215,212,210,208,205,203,200,198,195,192,190,187,184,181,
  178,176,173,170,167,164,161,158,155,152,149,146,143,139,136,133,130,
  127,124,121,118,115,111,108,105,102,99,96,93,90,87,84,81,78,
  76,73,70,67,64,62,59,56,54,51,49,46,44,42,39,37,35,33,31,29,27,25,23,
  21,20,18,16,15,14,12,11,10,9,7,6,5,5,4,3,2,2,1,1,1,0,0,0,0,0,0,0,1,1,
  1,2,2,3,4,5,5,6,7,9,10,11,12,14,15,16,18,20,21,23,25,27,29,31,
  33,35,37,39,42,44,46,49,51,54,56,59,62,64,67,70,73,76,78,81,84,87,90,
  93,96,99,102,105,108,111,115,118,121,124
};

/*PROGMEM const unsigned char saw8_256[] = 
{127,130,133,136,139,143,146,149,152,155,158,161,164,167,170,173,176,178,181,184,187,190,192,195,198,200,203,205,208,210,212,215,217,219,221,223,225,227,229,231,233,234,236,238,239,240,242,243,244,245,247,248,249,249,250,251,252,252,253,253,253,254,254,254,254,254,254,254,253,253,253,252,252,251,250,249,249,248,247,245,244,243,242,240,239,238,236,234,233,231,229,227,225,223,221,219,217,215,212,210,208,205,203,200,198,195,192,190,187,184,181,178,176,173,170,167,164,161,158,155,152,149,146,143,139,136,133,130,127,124,121,118,115,111,108,105,102,99,96,93,90,87,84,81,78,76,73,70,67,64,62,59,56,54,51,49,46,44,42,39,37,35,33,31,29,27,25,23,21,20,18,16,15,14,12,11,10,9,7,6,5,5,4,3,2,2,1,1,1,0,0,0,0,0,0,0,1,1,1,2,2,3,4,5,5,6,7,9,10,11,12,14,15,16,18,20,21,23,25,27,29,31,33,35,37,39,42,44,46,49,51,54,56,59,62,64,67,70,73,76,78,81,84,87,90,93,96,99,102,105,108,111,115,118,121,124,
127,131,135,140,144,148,152,156,161,165,169,173,177,180,184,188,192,195,199,202,205,209,212,215,218,221,223,226,228,231,233,235,237,239,241,243,244,246,247,248,250,251,251,252,253,253,254,254,254,254,254,254,253,253,253,252,251,250,249,249,247,246,245,244,242,241,239,238,236,234,233,231,229,227,225,223,221,219,217,215,213,211,209,207,205,203,201,198,196,194,192,190,188,186,184,182,180,178,176,174,172,170,168,166,164,163,161,159,157,156,154,152,151,149,148,146,144,143,141,140,138,137,136,134,133,131,130,128,127,126,124,123,121,120,118,117,116,114,113,111,110,108,106,105,103,102,100,98,97,95,93,91,90,88,86,84,82,80,78,76,74,72,70,68,66,64,62,60,58,56,53,51,49,47,45,43,41,39,37,35,33,31,29,27,25,23,21,20,18,16,15,13,12,10,9,8,7,5,5,4,3,2,1,1,1,0,0,0,0,0,0,1,1,2,3,3,4,6,7,8,10,11,13,15,17,19,21,23,26,28,31,33,36,39,42,45,49,52,55,59,62,66,70,74,77,81,85,89,93,98,102,106,110,114,119,123,
127,133,139,146,152,158,164,169,175,181,186,192,197,202,206,211,215,219,223,227,231,234,237,240,242,244,246,248,250,251,252,253,253,254,254,254,254,253,253,252,251,250,249,248,247,245,244,242,241,239,237,235,233,232,230,228,226,224,222,220,218,217,215,213,211,209,208,206,204,203,201,200,198,197,195,194,192,191,190,188,187,186,184,183,182,181,179,178,177,176,175,173,172,171,170,168,167,166,165,163,162,161,160,158,157,156,155,153,152,151,150,148,147,146,144,143,142,141,139,138,137,136,134,133,132,131,129,128,127,126,125,123,122,121,120,118,117,116,115,113,112,111,110,108,107,106,104,103,102,101,99,98,97,96,94,93,92,91,89,88,87,86,84,83,82,81,79,78,77,76,75,73,72,71,70,68,67,66,64,63,62,60,59,57,56,54,53,51,50,48,46,45,43,41,39,37,36,34,32,30,28,26,24,22,21,19,17,15,13,12,10,9,7,6,5,4,3,2,1,1,0,0,0,0,1,1,2,3,4,6,8,10,12,14,17,20,23,27,31,35,39,43,48,52,57,62,68,73,79,85,90,96,102,108,115,121,
127,137,147,157,167,176,185,194,202,209,216,223,229,234,238,242,246,248,250,252,253,254,254,254,253,252,251,250,249,247,245,243,242,240,238,236,235,233,231,230,228,227,226,224,223,222,221,220,219,217,216,215,214,213,212,211,209,208,207,206,205,203,202,201,200,199,197,196,195,194,193,191,190,189,188,187,186,185,183,182,181,180,179,178,177,176,174,173,172,171,170,169,168,166,165,164,163,162,161,160,158,157,156,155,154,153,152,151,149,148,147,146,145,144,143,142,141,139,138,137,136,135,134,133,131,130,129,128,127,126,125,124,123,121,120,119,118,117,116,115,113,112,111,110,109,108,107,106,105,103,102,101,100,99,98,97,96,94,93,92,91,90,89,88,86,85,84,83,82,81,80,78,77,76,75,74,73,72,71,69,68,67,66,65,64,63,61,60,59,58,57,55,54,53,52,51,49,48,47,46,45,43,42,41,40,39,38,37,35,34,33,32,31,30,28,27,26,24,23,21,19,18,16,14,12,11,9,7,5,4,3,2,1,0,0,0,1,2,4,6,8,12,16,20,25,31,38,45,52,60,69,78,87,97,107,117,
127,145,162,179,194,208,220,230,239,245,249,252,254,254,253,252,251,249,247,245,243,242,241,239,238,237,236,235,234,233,232,231,230,229,227,226,225,224,223,222,221,220,219,217,216,215,214,213,212,211,210,209,208,207,205,204,203,202,201,200,199,198,197,196,195,194,193,192,191,189,188,187,186,185,184,183,182,181,180,179,178,177,176,175,174,172,171,170,169,168,167,166,165,164,163,162,161,160,159,158,157,156,154,153,152,151,150,149,148,147,146,145,144,143,142,141,140,139,138,137,135,134,133,132,131,130,129,128,127,126,125,124,123,122,121,120,119,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,98,97,96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,65,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,47,46,45,44,43,42,41,40,39,38,37,35,34,33,32,31,30,29,28,27,25,24,23,22,21,20,19,18,17,16,15,13,12,11,9,7,5,3,2,1,0,0,2,5,9,15,24,34,46,60,75,92,109,
127,160,190,216,234,246,252,254,253,251,249,247,246,245,244,243,242,241,240,239,238,237,236,235,234,232,231,230,229,228,227,226,225,224,223,222,221,220,219,218,217,216,215,214,213,212,211,210,209,208,207,206,205,204,203,202,201,199,198,197,196,195,194,193,192,191,190,189,188,187,186,185,184,183,182,181,180,179,178,177,176,175,174,173,172,171,170,169,168,167,166,165,164,163,162,161,160,159,158,157,156,155,154,153,151,150,149,148,147,146,145,144,143,142,141,140,139,138,137,136,135,134,133,132,131,130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,101,100,99,98,97,96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,65,64,63,62,61,60,59,58,57,56,55,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,20,19,18,17,16,15,14,13,12,11,10,9,8,7,5,3,1,0,2,8,20,38,64,94,
127,189,232,251,254,251,249,249,248,247,246,245,244,243,241,240,239,238,237,236,235,234,233,232,231,230,229,228,227,226,225,224,223,222,221,220,219,218,217,216,215,214,213,212,211,210,209,208,207,206,205,204,203,202,201,200,199,198,197,196,195,194,193,192,191,190,189,188,187,186,185,184,183,182,181,180,179,178,177,176,175,174,173,172,171,170,169,168,167,166,165,164,163,162,161,160,159,158,157,156,155,154,153,152,151,150,149,148,147,146,145,144,143,142,141,140,139,138,137,136,135,134,133,132,131,130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,65,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,11,10,9,8,7,6,5,5,3,0,3,22,65,
127,231,254,251,250,249,248,247,246,245,244,243,242,241,240,239,238,237,236,235,234,233,232,231,230,229,228,227,226,225,224,223,222,221,220,219,218,217,216,215,214,213,212,211,210,209,208,207,206,205,204,203,202,201,200,199,198,197,196,195,194,193,192,191,190,189,188,187,186,185,184,183,182,181,181,180,179,178,177,176,175,174,173,172,171,170,169,168,167,166,165,164,163,162,161,160,159,158,157,156,155,154,153,152,151,150,149,148,147,146,145,144,143,142,141,140,139,138,137,136,135,134,133,132,131,130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,73,72,71,70,69,68,67,66,65,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,0,23,
};*/

PROGMEM const unsigned char saw8_256[] = {128,129,131,133,134,136,138,139,141,143,144,146,148,149,151,152,154,156,157,159,160,162,163,165,166,167,169,170,171,173,174,175,177,178,179,180,181,182,183,184,185,186,187,188,189,189,190,191,192,192,193,193,194,194,195,195,195,196,196,196,196,197,197,197,197,197,197,197,196,196,196,196,195,195,195,194,194,193,193,192,192,191,190,189,189,188,187,186,185,184,183,182,181,180,179,178,177,175,174,173,171,170,169,167,166,165,163,162,160,159,157,156,154,152,151,149,148,146,144,143,141,139,138,136,134,133,131,129,127,126,124,122,121,119,117,116,114,112,111,109,107,106,104,103,101,99,98,96,95,93,92,90,89,88,86,85,84,82,81,80,78,77,76,75,74,73,72,71,70,69,68,67,66,66,65,64,63,63,62,62,61,61,60,60,60,59,59,59,59,58,58,58,58,58,58,58,59,59,59,59,60,60,60,61,61,62,62,63,63,64,65,66,66,67,68,69,70,71,72,73,74,75,76,77,78,80,81,82,84,85,86,88,89,90,92,93,95,96,98,99,101,103,104,106,107,109,111,112,114,116,117,119,121,122,124,126,128,131,134,138,141,144,148,151,154,157,161,164,167,170,173,176,179,181,184,186,189,191,194,196,198,200,202,204,205,207,209,210,211,212,213,214,215,216,216,217,217,217,217,218,217,217,217,217,216,215,215,214,213,212,211,210,209,207,206,205,203,202,200,198,197,195,193,192,190,188,186,184,182,180,178,176,175,173,171,169,167,165,163,162,160,158,156,155,153,152,150,149,147,146,144,143,142,141,140,138,137,137,136,135,134,133,133,132,131,131,130,130,130,129,129,129,128,128,128,128,128,128,128,128,128,128,128,128,128,127,127,127,127,127,127,127,127,127,127,127,127,126,126,126,125,125,125,124,124,123,122,122,121,120,119,118,118,117,115,114,113,112,111,109,108,106,105,103,102,100,99,97,95,93,92,90,88,86,84,82,80,79,77,75,73,71,69,67,65,63,62,60,58,57,55,53,52,50,49,48,46,45,44,43,42,41,40,40,39,38,38,38,38,37,38,38,38,38,39,39,40,41,42,43,44,45,46,48,50,51,53,55,57,59,61,64,66,69,71,74,76,79,82,85,88,91,94,98,101,104,107,111,114,117,121,124,128,134,141,148,154,161,167,173,179,185,191,196,201,205,210,214,217,220,223,226,228,230,231,232,233,233,233,233,232,232,230,229,227,226,224,222,219,217,214,212,209,207,204,202,199,197,194,192,190,188,186,184,182,181,180,178,177,176,176,175,174,174,174,174,174,174,174,174,174,174,175,175,175,175,176,176,176,176,176,176,176,175,175,174,174,173,172,171,170,169,167,166,165,163,162,160,158,156,155,153,151,149,148,146,144,143,141,140,138,137,136,135,134,133,132,131,130,130,129,129,128,128,128,128,128,128,128,128,128,127,127,127,127,127,127,127,127,126,126,125,125,124,123,122,121,120,119,118,117,115,114,112,111,109,107,106,104,102,100,99,97,95,93,92,90,89,88,86,85,84,83,82,81,81,80,80,79,79,79,79,79,79,79,80,80,80,80,81,81,81,81,81,81,81,81,81,81,80,79,79,78,77,75,74,73,71,69,67,65,63,61,58,56,53,51,48,46,43,41,38,36,33,31,29,28,26,25,23,23,22,22,22,22,23,24,25,27,29,32,35,38,41,45,50,54,59,64,70,76,82,88,94,101,107,114,121,128,141,154,167,180,191,202,212,220,227,233,237,241,243,243,243,242,240,237,234,230,226,222,218,214,211,208,205,203,201,200,199,199,199,200,201,202,203,204,205,206,206,207,207,206,206,205,203,202,200,198,196,194,191,189,187,185,183,182,180,179,179,178,178,178,178,178,178,179,179,179,179,179,179,178,178,177,176,174,173,171,169,168,166,164,162,160,159,158,156,155,154,154,153,153,153,153,153,153,153,153,153,153,152,152,151,150,149,148,147,145,144,142,140,139,137,135,134,132,131,130,129,129,128,128,128,128,128,127,127,127,127,127,127,126,126,125,124,123,121,120,118,116,115,113,111,110,108,107,106,105,104,103,103,102,102,102,102,102,102,102,102,102,102,101,101,100,99,97,96,95,93,91,89,87,86,84,82,81,79,78,77,77,76,76,76,76,76,76,77,77,77,77,77,77,76,76,75,73,72,70,68,66,64,61,59,57,55,53,52,50,49,49,48,48,49,49,50,51,52,53,54,55,56,56,56,55,54,52,50,47,44,41,37,33,29,25,21,18,15,13,12,12,12,14,18,22,28,35,43,53,64,75,88,101,114,128,154,180,203,221,235,244,249,249,246,240,233,227,221,216,213,212,213,215,218,220,223,224,224,223,221,218,214,211,208,206,204,204,204,205,206,208,208,208,208,206,204,202,199,197,195,193,193,192,193,193,194,194,194,194,193,191,189,187,185,183,182,181,180,180,180,180,180,181,180,180,178,177,175,173,171,170,168,167,167,167,167,167,167,167,166,166,164,163,161,159,157,156,155,154,154,154,154,154,154,153,153,152,150,149,147,145,144,142,142,141,141,141,141,141,140,140,139,138,136,135,133,131,130,129,128,128,128,127,127,127,127,126,125,124,122,120,119,117,116,115,115,114,114,114,114,114,113,113,111,110,108,106,105,103,102,102,101,101,101,101,101,101,100,99,98,96,94,92,91,89,89,88,88,88,88,88,88,88,87,85,84,82,80,78,77,75,75,74,75,75,75,75,75,74,73,72,70,68,66,64,62,61,61,61,61,62,62,63,62,62,60,58,56,53,51,49,47,47,47,47,49,50,51,51,51,49,47,44,41,37,34,32,31,31,32,35,37,40,42,43,42,39,34,28,22,15,9,6,6,11,20,34,52,75,101,128,180,222,246,252,245,233,223,219,222,228,233,233,230,224,219,217,219,222,224,224,221,217,214,212,213,215,216,216,213,210,207,207,207,208,209,208,206,203,201,200,201,202,202,201,199,196,194,194,194,195,195,194,192,189,188,187,188,188,188,187,185,183,181,181,181,181,181,180,178,176,175,174,174,174,174,173,171,169,168,168,168,168,167,166,164,162,161,161,161,161,160,159,157,155,154,154,154,154,153,152,150,149,148,148,148,147,146,145,143,142,141,141,141,140,139,138,136,135,134,134,134,134,133,131,129,128,128,128,127,127,126,124,122,121,121,121,121,120,119,117,116,115,114,114,114,113,112,110,109,108,107,107,107,106,105,103,102,101,101,101,101,100,98,96,95,94,94,94,94,93,91,89,88,87,87,87,87,86,84,82,81,81,81,81,80,79,77,75,74,74,74,74,74,72,70,68,67,67,67,68,67,66,63,61,60,60,61,61,61,59,56,54,53,53,54,55,54,52,49,47,46,47,48,48,48,45,42,39,39,40,42,43,41,38,34,31,31,33,36,38,36,31,25,22,22,27,33,36,32,22,10,3,9,33,75,128,222,254,236,222,232,238,230,224,229,232,227,223,226,228,223,220,222,223,220,217,219,220,216,214,216,216,213,211,212,212,209,208,209,209,206,205,205,205,203,201,202,202,199,198,199,198,196,195,195,195,192,191,192,191,189,188,188,188,186,185,185,184,182,181,182,181,179,178,178,177,175,175,175,174,172,171,171,170,169,168,168,167,165,165,165,164,162,161,161,160,158,158,158,157,155,154,154,153,152,151,151,150,148,148,148,146,145,144,144,143,141,141,141,140,138,138,137,136,135,134,134,133,131,131,131,129,128,127,127,126,124,124,124,122,121,121,120,119,118,117,117,115,114,114,114,112,111,111,110,109,107,107,107,105,104,104,103,102,101,101,100,98,97,97,97,95,94,94,93,91,90,90,90,88,87,87,86,85,84,84,83,81,80,80,80,78,77,77,76,74,73,74,73,71,70,70,69,67,67,67,66,64,63,64,63,60,60,60,59,57,56,57,56,53,53,54,52,50,50,50,49,46,46,47,46,43,43,44,42,39,39,41,39,35,36,38,35,32,33,35,32,27,29,32,28,23,26,31,25,17,23,33,19,1,33,128,255,224,241,228,236,228,234,227,231,226,229,224,227,223,225,221,223,220,221,218,220,217,218,215,216,213,214,212,212,210,211,208,209,207,207,205,205,204,204,202,202,200,200,198,199,197,197,195,195,193,193,192,192,190,190,188,188,187,186,185,185,183,183,182,181,180,180,178,178,177,176,175,174,173,173,172,171,170,169,168,168,166,166,165,164,163,163,161,161,160,159,158,157,156,156,155,154,153,152,151,151,150,149,148,147,146,145,144,144,143,142,141,140,139,139,138,137,136,135,134,133,133,132,131,130,129,128,128,127,126,125,124,123,122,122,121,120,119,118,117,116,116,115,114,113,112,111,111,110,109,108,107,106,105,104,104,103,102,101,100,99,99,98,97,96,95,94,94,92,92,91,90,89,89,87,87,86,85,84,83,82,82,81,80,79,78,77,77,75,75,74,73,72,72,70,70,69,68,67,67,65,65,63,63,62,62,60,60,58,58,56,57,55,55,53,53,51,51,50,50,48,48,46,47,44,45,43,43,41,42,39,40,37,38,35,37,34,35,32,34,30,32,28,31,26,29,24,28,21,27,19,27,14,31,0,};

/*
  Wavetables are stored in the array from the one with least harmonics to the one with most.
  There are 8 wavetables. Highest has only fundamental, second has fundamental + 1 harmonic etc.
  wavetableIndexPointer stores the beginning address of the wavetable being used.
  wavetableMorphRatio holds value that is used in mixing two wavetables together.
*/

volatile unsigned int wavetableIndexPointer = 0;
volatile byte wavetableMorphRatio = 0;

/*
  Serial API documentation !!!
 
 NOTE: All bytes (but the command byte) should always have 0 as the first bit followed by 7-bit value
 (range will then be from 0-127). For values split to two bytes, two similarly composed 7-bit
 values are to be used to represent most and least significant bytes.
 */

/*                                      byte 0 | byte 1  | byte 2        | byte 3        | byte 4   */
#define SET_FREQUENCY  0xF0             // 240 | address | frequency LSB | frequency MSB
#define SET_VELOCITY   0xF1             // 241 | address | volume
#define SET_MASTER_VOLUME 0xF2          // 242 | address | volume
#define ADDRESS_ASSIGN 0xF3             // 243 | address
#define SET_FREQUENCY_AND_VELOCITY 0xF4 // 244 | address | frequency LSB | frequency MSB | volume
#define SET_WAVEFORM        0xF5        // 245 | address | waveform (check values below)
#define SET_LED             0xF6        // 246 | address | LED # (0/1) | brightness (0 = off, other values = on)
#define SET_FADE_SPEED      0xF7        // 247 | address | speed LSB | speed MSB (speed = volume units / second)
#define SET_PORTAMENTO      0xF8        // 248 | address | speed LSB | speed MSB (speed = frequency units / second)
#define SET_LP_FILTER       0xF9        // 249 | address | on (0 = off, other values = on)
#define RESET_NEXT          0xFA        // 250 | address | include address assignment (0=no, other values = yes)

#define WAVEFORM_SQUARE        0x00     // 1
#define WAVEFORM_SAW           0x01     // 2
#define WAVEFORM_TRIANGLE      0x02     // 3
#define WAVEFORM_NOISE         0x03     // 4
#define WAVEFORM_SINE          0x04     // 5
#define WAVEFORM_COLORED_NOISE 0x05     // 6
#define WAVEFORM_BL_SAW        0x06     // 7
#define WAVEFORM_BL_SQUARE     0x07     // 8 Totally fucked up

/* DCO section */
const int refclk=15625;                                           // effective sample rate
volatile unsigned long frequencyPointer = 0;                      // read sample position
volatile unsigned long frequencyReadSpeed = pow(2,32)*440/refclk; // read speed
volatile byte volume = 0;               // This is effective volume used by synthesis engine [0 - 255]
volatile unsigned int preVolume = 0;    // This is volume used in portamentos
volatile unsigned int postVolume = 255; // This is main attenuator volume
byte waveform = WAVEFORM_NOISE;
boolean interpolation = false;
boolean mute = false;

/* Portamento section */
byte targetVolume = 0; // This is volume where preVolume is gliding to
unsigned int portamentoSpeed = 0;  // ms / volume_unit (8bit)
unsigned long targetReadSpeed = frequencyReadSpeed;
unsigned int fPortamentoSpeed = 0; // ms / volume_unit (8bit)

/*                                                               _
                                                              _( (~\
       _ _                        /                          ( \> > \
   -/~/ / ~\                     :;                \       _  > /(~\/
  || | | /\ ;\                   |l      _____     |;     ( \/ /   /
  _\\)\)\)/ ;;;                  `8o __-~     ~\   d|      \   \  //
 ///(())(__/~;;\                  "88p;.  -. _\_;.oP        (_._/ /
(((__   __ \\   \                  `>,% (\  (\./)8"         ;:'  i
)))--`.'-- (( ;,8 \               ,;%%%:  ./V^^^V'          ;.   ;.
((\   |   /)) .,88  `: ..,,;;;;,-::::::'_::\   ||\         ;[8:   ;
 )|  ~-~  |(|(888; ..``'::::8888oooooo.  :\`^^^/,,~--._    |88::| |
  \ -===- /|  \8;; ``:.      oo.8888888888:`((( o.ooo8888Oo;:;:'  |
 |_~-___-~_|   `-\.   `        `o`88888888b` )) 888b88888P""'     ;
  ;~~~~;~~         "`--_`.       b`888888888;(.,"888b888"  ..::;-'
   ;      ;              ~"-....  b`8888888:::::.`8888. .:;;;''
      ;    ;                 `:::. `:::OOO:::::::.`OO' ;;;''
 :       ;                     `.      "``::::::''    .'
    ;                           `.   \_              /
  ;       ;                       +:   ~~--  `:'  -';
                                   `:         : .::/
      ;                            ;;+_  :::. :..;;;
*/

void setup() {
  Serial.begin(115200);
  pinMode(EXCTR_LED,OUTPUT);
  pinMode(EXCTR_LED + 1,OUTPUT);
  analogWrite(EXCTR_LED + 1,50);
  updateWavetableScan();
  initPwmOutput();
}


void loop() {
  processTraffic();
  calculateVolumeEnvelope();
  calculateFrequencyEnvelope();
}


long lastMessageReceivedAt = millis();
void processTraffic() {
  if (millis()-lastMessageReceivedAt > CONNECTION_LOST_TIMEOUT) { 
    mute = true;
  }

  if (Serial.available() < 1) return;
  lastMessageReceivedAt = millis();
  mute = false;

  byte command = Serial.read();
  if ((command & B10000000) != 0x80) { // == data byte
    return;
  } 

  byte addr = read1bitValue(false);
  if (addr > 127) return;

  if (command == ADDRESS_ASSIGN) {
    address = addr;
    /* Sending the assigment message to the next board */
    if (!PARALLEL_MODE) {
      Serial.write(command);
      Serial.write(address+1);
    }
    //digitalWrite(EXCTR_LED,LOW);
    digitalWrite(EXCTR_LED+1,LOW);
    //digitalWrite(ARDUINO_LED,LOW);
    return; 
  }

  /* Now that we have an address, we are checking whether the command
   is meant for us or not. */
  if (addr == 127) {
    /* broadcasting mode! Commands will be executed and forwarded to the next node. */
    broadcasting = true;
    Serial.write(command);
    Serial.write(addr);
  } else {
    broadcasting = false;  
  }
   
  if (addr != address && !broadcasting) {
    /* forwarding */
    if (!PARALLEL_MODE) {
        Serial.write(command);
        Serial.write(addr);
    }
    while (true) {
      if (!byteReady()) return;
      if ((Serial.peek() & B10000000) != 0x80) { // == data byte
        byte r = Serial.read();
        if (!PARALLEL_MODE) {
            Serial.write(r);
        }  
      } 
      else {
        return;  
      }
    }
  } 
  else {
    switch (command) {
    case SET_FREQUENCY: 
      {
        int frequency = read2bitValue(broadcasting);
        if (frequency > 0) {
          targetReadSpeed = pow(2,32)*frequency/refclk;
          updateWavetableScan();
        } 
        else {
          return;  
        }
      }
      break;
    case SET_VELOCITY:
      {
        byte v = read1bitValue(broadcasting);
        if (v==255) return;
        
        if (volume == 0) {
          frequencyPointer = 0;  
        }
        
        targetVolume = v*2 + 1;
      }
      break;
    case SET_FREQUENCY_AND_VELOCITY:
      {
        int frequency = read2bitValue(broadcasting);
        if (frequency < 0) return;  
        
        byte v = read1bitValue(broadcasting);
        if (v==255) return;
        
        if (volume == 0) {
          frequencyPointer = 0;  
        }
        targetVolume = v*2 + 1;
        //targetVolume = (preVolume * (postVolume + 1)) >> 8; 
        targetReadSpeed = pow(2,32)*frequency/refclk; //frequency;
        
        /*if (fPortamentoSpeed==0)
          frequencyReadSpeed = targetReadSpeed;
        updateWavetableScan();*/
      }
      break;
    case SET_WAVEFORM:
      {
        byte w = read1bitValue(broadcasting);
        if (w==255) return;
        switch (w) {
        case WAVEFORM_SQUARE:
        case WAVEFORM_SAW:
        case WAVEFORM_TRIANGLE:
        case WAVEFORM_NOISE:
        case WAVEFORM_SINE:
        case WAVEFORM_COLORED_NOISE:
        case WAVEFORM_BL_SAW:
        case WAVEFORM_BL_SQUARE:
          if (waveform!=w) {
            waveform = w;
            frequencyPointer = 0;
          }
          break;
        }
      }
      break;
    case SET_FADE_SPEED: // 0-1000 ?
      {
        /*
          A bit funny one...
          value is 1 volume unit / second. For example:
          1 => fades from 0 to 127 in 127 * 2 / 1 (4,25 minutes) seconds.
          999 => fades from 0 to 127 in 127 * 2 / 999 (0.25)seconds.
          0 => does not move at all
          1000 => jumps immediately
          For whatever reason we are inverting this internally so in
          portamentoSpeed everything is inverted!!!
        */
        int ps = read2bitValue(broadcasting); // These are milliseconds / volume unit
        if (ps < 0) return;
        if (ps > 1000) { // There are 256 volume steps so can't go faster
          ps = 1000;  
        }
        portamentoSpeed = 1000 - ps;
      }
      break;
    case SET_PORTAMENTO:
      {
        int ps = read2bitValue(broadcasting); // freq_units (7bit!!!) / s
        if (ps < 0) return;
        
        if (ps == 0) { // portamento off!
          fPortamentoSpeed = 0;
          frequencyReadSpeed = targetReadSpeed;
        } else if (ps > 8000) {
          fPortamentoSpeed = 8000;
          //frequencyReadSpeed = targetReadSpeed;    
        }
        else {
          fPortamentoSpeed = ps;
        }
      }
      break;
    case SET_LED:
      {
        byte l = read1bitValue(broadcasting);
        if (l > 1) return;
        
        byte v = read1bitValue(broadcasting);
        if (v==255) return;
        
        if (v > 0)
          analogWrite(EXCTR_LED + l, v * 2);
        else
          digitalWrite(EXCTR_LED + l, LOW); 
      }
      break;
    case SET_LP_FILTER:
      {
        byte v = read1bitValue(broadcasting);
        if (v==255) return;
        
        if (v > 0)
          interpolation = true;
        else 
          interpolation = false; 
      }
      break;
    case SET_MASTER_VOLUME:
      {
        byte v = read1bitValue(broadcasting);
        if (v==255) return;
        
        if (volume == 0) {
          frequencyPointer = 0;  
        }
        
        postVolume = v*2 + 1;
        //targetVolume = (preVolume * (postVolume + 1)) >> 8; 
        if (portamentoSpeed==0)
          volume = targetVolume;
      }
      break;
    case RESET_NEXT:
      {
        if (!byteReady()) return;
        if ((Serial.peek() >> 7) > 0) return; 
        int w = Serial.read();
        digitalWrite(RESETER_PIN, LOW);
        delay(RESET_DELAY);
        digitalWrite(RESETER_PIN, HIGH);
        if (w > 0) {
          delay(BOOT_DELAY);
          byte tmp = ADDRESS_ASSIGN;
          Serial.write(tmp);
          Serial.write(address+1);
        } 
      }
      break;
    }
  }
}


unsigned long lastVolumeUpdate = millis();
void calculateVolumeEnvelope() {
  if (targetVolume != volume) {
    if (portamentoSpeed == 0) {
      preVolume = targetVolume; 
      volume = (preVolume * (postVolume + 1)) >> 8; 
    } else if (portamentoSpeed < 1000) {
      if (millis() - lastVolumeUpdate > portamentoSpeed) {
        lastVolumeUpdate = millis();
        if (targetVolume > preVolume) {
          preVolume++;
          volume = (preVolume * (postVolume + 1)) >> 8; 
        } 
        else if (targetVolume < preVolume) {
          preVolume--;
          volume = (preVolume * (postVolume + 1)) >> 8; 
        } 
      }
    } 
  }
}


/* DONT LOOK, I DID NOT MAKE THIS!!! */
/*
  I think it works such that the frequencyReadSpeed will jump fixed steps
  every interval defined by fPortamentoSpeed (ms) towards the targetReadSpeed.
*/
unsigned long lastFrequencyUpdate = millis();
void calculateFrequencyEnvelope() {
  if (targetReadSpeed - frequencyReadSpeed != 0 && frequencyReadSpeed - targetReadSpeed != 0) {
    if (fPortamentoSpeed == 0) {
      frequencyReadSpeed = targetReadSpeed;
      updateWavetableScan();    
    } else if (fPortamentoSpeed > 0 && fPortamentoSpeed < 8000) {
      if (millis() - lastFrequencyUpdate > fPortamentoSpeed) {
        lastFrequencyUpdate = millis();
        if (targetReadSpeed > frequencyReadSpeed) {
          frequencyReadSpeed+=0xFFFF;
          if (targetReadSpeed < frequencyReadSpeed) {
            frequencyReadSpeed = targetReadSpeed;
            updateWavetableScan(); 
          }
        } 
        else if (frequencyReadSpeed > targetReadSpeed) {
          frequencyReadSpeed-=0xFFFF;
          if (targetReadSpeed > frequencyReadSpeed) {
            frequencyReadSpeed = targetReadSpeed; 
            updateWavetableScan();
          }
        }        
      }
    } 
  }
}


/*
  Reads 2 7-bit values from Serial and combines them to one (unsigned) int.
 -1 is returned if value could not be read.
 if b is true, value will also be written to the Serial line.
 */
int read2bitValue(boolean b) {
  if (!byteReady()) return -1;
  if ((Serial.peek() >> 7) > 0) return -1; 
  byte lsb = Serial.read();
  if (!byteReady()) return -1;
  if ((Serial.peek() >> 7) > 0) return -1; 
  byte msb = Serial.read();
  int value = msb * 128 + lsb;
  if (b) {
    Serial.write(lsb);
    Serial.write(msb);    
  } 
  return value;
}

// returns 255 if something went wrong
byte read1bitValue(boolean b) {
  if (!byteReady()) return 255;
  if ((Serial.peek() >> 7) > 0) return 255; 
  byte v = Serial.read();
  if (b)
    Serial.write(v); 
  return v;  
}


void send2bitValue(int i) {
  if (i > 16383 || i < 0) return;
  byte lsb = i & 0x7F;
  byte msb = i >> 7;
  Serial.write(lsb);
  Serial.write(msb);
}

/*
                                           ,_
                           , . ,          <d_)_
                         `@*@*@*@'        \(_>///
                        '/(((/)))),       .'__,'
                      `-/6())c '(\\-    .' /
                      /((()(   _/(8)__.'  / 
                      9/)9))) (((9((    .'
                    '/()(((8)  `)())---'
                    `9))()9())\(6(()\,        ,                   <)()
                    / `((6(( _/_/` '       ,o<>oo,,      ()`8o   () 8y
                   /   ')))`   |       `8o ()o ()<>o,   8\.o8 <) \8oo'
                  /   .' \     |      `()8()()8()()8,  8()((/   \/y-(>
        ()()     /  .'    )    |       8\_8hjw\8 \y 8,  `-))-8o||/ ,
         \/     / .'     /     |`-.   <)-'<>\\ \\/8 o,   //   _//_()8,
     <)._/-'  _/ /      /      )   `-.   8()-\\// 8()o  /(_.-' ,--8<>8o
      \_/  __/  ,\__    |      \      `.    ,','-'    _/     .88()o'
       `--' ///|    `-.__\      \.___   \--' /____.--'  .'`-.`._/_.-<>
             '            `.     \   \   \        (O)  / ,()_`.\ o
        //\\\       ((O))   `.    \   `.  \       __.-'  ()8 /,8\-8
       (((O)))                )   /     `. \_.---'\,        /\  ()`
        \\///    _/    ___.-.'   /--.__.--`.\     /\_      ()()
         .--.___./----'    /    /          ( \     '\
       ,'      _/\_,(>    /   .'            ) \
      /        / '\      /  .'              `-.\
              (   (>   _/ .'                   `'
                      (  /
                       )/
                      (/
*/

/*
  A naive DDS oscillator implementation
 */
volatile byte sample = 0;
volatile byte sample_n = 0;
volatile byte output = 0;
byte div4 = 0;
ISR(TIMER2_OVF_vect) { 
  OCR2A = output; // Lets write the sample we calculated during previous iteration (to reduce jitter)    
  if (!(div4++%4)) {
    frequencyPointer += frequencyReadSpeed;
    switch (waveform) {     
    case WAVEFORM_SQUARE:     
      sample = (frequencyPointer >> 31) * volume;
      break;
    case WAVEFORM_SAW:
      sample = (((frequencyPointer >> 24)*(volume+1)) >> 8);
      break;
    case WAVEFORM_TRIANGLE:
      if (frequencyPointer >> 31) { 
        sample = ((((frequencyPointer >> 23) & B11111111)*(volume+1)) >> 8);
      } 
      else {
        sample = (((0xFF - ((frequencyPointer >> 23) & 0xFF))*(volume+1)) >> 8 );
      }
      break;
    case WAVEFORM_SINE:
      sample = ((unsigned int)pgm_read_byte_near(sine256 + (frequencyPointer >> 24)) * (volume+1)) >> 8;
      break;
    case WAVEFORM_NOISE:
      sample = random(volume);
      break;
    case WAVEFORM_COLORED_NOISE:
      if (frequencyPointer >> 31 != (frequencyPointer + frequencyReadSpeed) >> 31)
        sample = random(volume);
      break;
    case WAVEFORM_BL_SAW:
      if (wavetableMorphRatio > 0 ) {
        sample = (((
                 (((((unsigned int)pgm_read_byte_near(saw8_256 + wavetableIndexPointer + (frequencyPointer >> 24))))*(256-wavetableMorphRatio))>>8) +
                 (((((unsigned int)pgm_read_byte_near(saw8_256 + (wavetableIndexPointer-256) + (frequencyPointer >> 24))))*(wavetableMorphRatio+1))>>8)                 
                 )*(volume+1))>>8);
      } else
        sample = (((pgm_read_byte_near(saw8_256 + wavetableIndexPointer + (frequencyPointer >> 24)))*(volume+1))>>8);
      break;
    case WAVEFORM_BL_SQUARE:
      sample = (((-(pgm_read_byte_near(saw8_256 + wavetableIndexPointer + (frequencyPointer >> 24))) + (pgm_read_byte_near(saw8_256 + wavetableIndexPointer + (((frequencyPointer >> 24)+127)%255))))>>2 *volume)>>8);
      break;
    }

    if (interpolation) {
      output = ((sample + sample_n) >> 1);
      sample_n = sample;
    } 
    else {
      output = sample;  
    }
  }
}


void initPwmOutput() {
  // We are using Timer 2
  sbi (TIMSK2,TOIE2); // Setting interupt type to OVF?

  // Setting TCCR2B CS values to 001 == no prescaling
  sbi (TCCR2B, CS20); 
  cbi (TCCR2B, CS21);
  cbi (TCCR2B, CS22);

  // Setting TCCR2A COM2Ax values to 10 == none inverted on OC2A
  cbi (TCCR2A, COM2A0);
  sbi (TCCR2A, COM2A1);

  // Setting TCCR2A COM2Bx values to 1x == inverted/noninverted OC2B  
  sbi (TCCR2A, COM2B1);
  cbi (TCCR2A, COM2B0);

  // Setting TCCR2A wgm values to 011 == Fast PWM
  sbi (TCCR2A, WGM20);
  sbi (TCCR2A, WGM21);
  cbi (TCCR2B, WGM22);
  
  pinMode(11,OUTPUT);
}

/*
 false = timeout occurred, nothing to be read
 true  = byt can be read
 */
inline boolean byteReady() {
  long startTime = millis();
  while (Serial.available() < 1) {
    if (millis()-startTime > MAX_SERIAL_WAIT_MILLIS) {
      return false; 
    } 
  }
  return true;
}


void updateWavetableScan() {
  byte b = B10000000;
  byte s = frequencyReadSpeed >> 24;
  int newOffset = 0;
  while ( newOffset < 8 && ((b & s) == 0)) {
    b = b >> 1;
    newOffset++; 
  }
  if (newOffset == 8 || newOffset < 2) wavetableMorphRatio = 0;
  else
    wavetableMorphRatio = (byte)(frequencyReadSpeed >> (24 - (newOffset+1)));
  wavetableIndexPointer = max(0,(newOffset-1)*256);
}

